# **SALM (python)**
- - -
## **Introduction**
* ### Implemented a particle ﬁlter to track a humanoid robot’s 2D position and orientation using inertial measurement unit (IMU), odometry and laser measurements
* ### Built 2D occupancy grid maps of the environment using the results of the particle ﬁlter

## **Results**
* ### Map 1
![Alt text](img/map1.png)
* ### Map 2
![Alt text](img/map2.png)

## **Requirements**
* ### Python 3 
* ### [Training data](https://drive.google.com/open?id=0B241vEW29598Zm5LT241b2xLdWs) (from professor Atanasov)
* ### [Test data](https://drive.google.com/open?id=0B241vEW29598UTJTM2hnMnNfZGs) (from professor Atanasov)

## **Run**
* ### Put the program under ```src```
* ### Put the training data under ```trainset```
* ### Put the testing data under ```testset```
* ### Run the program with ```python main.py```